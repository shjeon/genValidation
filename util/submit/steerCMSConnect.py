import os

def steer(submitLines, gridpack, datasetname, runpointDir):

    os.system("cp {0} {1}".format(os.path.join("config", "submit", "cmsconnect.jds"), os.path.join(runpointDir, "cmsconnect.jds")))
    os.system("sed -i 's|\[gridpack\]|{0}|g' {1}".format(gridpack, os.path.join(runpointDir, "cmsconnect.jds")))
    os.system("sed -i 's|\[datasetname\]|{0}|g' {1}".format(datasetname, os.path.join(runpointDir, "cmsconnect.jds")))

    submitLines += "cd {0}\n".format(os.path.join(runpointDir))
    submitLines += "condor_submit cmsconnect.jds\n"
    submitLines += "cd -\n"

    return submitLines
